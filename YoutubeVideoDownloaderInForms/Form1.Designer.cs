﻿namespace YoutubeVideoDownloaderInForms
{
    partial class Form1 
    {
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.selectDownloadFolder = new MetroFramework.Controls.MetroButton();
            this.selectInputFile = new MetroFramework.Controls.MetroButton();
            this.downloadFolderPathTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.inputFilePathTextBox = new MetroFramework.Controls.MetroTextBox();
            this.inputFileExtensionTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.downloadButton = new MetroFramework.Controls.MetroButton();
            this.exitButton = new MetroFramework.Controls.MetroButton();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.clearmainTextBox = new MetroFramework.Controls.MetroButton();
            this.openDownloadFolder = new MetroFramework.Controls.MetroButton();
            this.checkBox01 = new MetroFramework.Controls.MetroCheckBox();
            this.advancedDownload = new MetroFramework.Controls.MetroButton();
            this.advancedOpenDownloadFolder = new MetroFramework.Controls.MetroButton();
            this.mainTextBox = new MetroFramework.Controls.MetroTextBox();
            this.testButton = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // selectDownloadFolder
            // 
            resources.ApplyResources(this.selectDownloadFolder, "selectDownloadFolder");
            this.selectDownloadFolder.Name = "selectDownloadFolder";
            this.selectDownloadFolder.UseSelectable = true;
            this.selectDownloadFolder.Click += new System.EventHandler(this.SelectDownloadFolder_Click_1);
            // 
            // selectInputFile
            // 
            resources.ApplyResources(this.selectInputFile, "selectInputFile");
            this.selectInputFile.Name = "selectInputFile";
            this.selectInputFile.UseSelectable = true;
            this.selectInputFile.Click += new System.EventHandler(this.SelectInputFile_Click);
            // 
            // downloadFolderPathTextBox
            // 
            // 
            // 
            // 
            this.downloadFolderPathTextBox.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.downloadFolderPathTextBox.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode")));
            this.downloadFolderPathTextBox.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location")));
            this.downloadFolderPathTextBox.CustomButton.Name = "";
            this.downloadFolderPathTextBox.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size")));
            this.downloadFolderPathTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.downloadFolderPathTextBox.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex")));
            this.downloadFolderPathTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.downloadFolderPathTextBox.CustomButton.UseSelectable = true;
            this.downloadFolderPathTextBox.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible")));
            this.downloadFolderPathTextBox.Lines = new string[0];
            resources.ApplyResources(this.downloadFolderPathTextBox, "downloadFolderPathTextBox");
            this.downloadFolderPathTextBox.MaxLength = 32767;
            this.downloadFolderPathTextBox.Name = "downloadFolderPathTextBox";
            this.downloadFolderPathTextBox.PasswordChar = '\0';
            this.downloadFolderPathTextBox.ReadOnly = true;
            this.downloadFolderPathTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.downloadFolderPathTextBox.SelectedText = "";
            this.downloadFolderPathTextBox.SelectionLength = 0;
            this.downloadFolderPathTextBox.SelectionStart = 0;
            this.downloadFolderPathTextBox.ShortcutsEnabled = true;
            this.downloadFolderPathTextBox.UseSelectable = true;
            this.downloadFolderPathTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.downloadFolderPathTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            resources.ApplyResources(this.metroLabel1, "metroLabel1");
            this.metroLabel1.Name = "metroLabel1";
            // 
            // metroLabel2
            // 
            resources.ApplyResources(this.metroLabel2, "metroLabel2");
            this.metroLabel2.Name = "metroLabel2";
            // 
            // inputFilePathTextBox
            // 
            // 
            // 
            // 
            this.inputFilePathTextBox.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            this.inputFilePathTextBox.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode1")));
            this.inputFilePathTextBox.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location1")));
            this.inputFilePathTextBox.CustomButton.Name = "";
            this.inputFilePathTextBox.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size1")));
            this.inputFilePathTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.inputFilePathTextBox.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex1")));
            this.inputFilePathTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.inputFilePathTextBox.CustomButton.UseSelectable = true;
            this.inputFilePathTextBox.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible1")));
            this.inputFilePathTextBox.Lines = new string[0];
            resources.ApplyResources(this.inputFilePathTextBox, "inputFilePathTextBox");
            this.inputFilePathTextBox.MaxLength = 32767;
            this.inputFilePathTextBox.Name = "inputFilePathTextBox";
            this.inputFilePathTextBox.PasswordChar = '\0';
            this.inputFilePathTextBox.ReadOnly = true;
            this.inputFilePathTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.inputFilePathTextBox.SelectedText = "";
            this.inputFilePathTextBox.SelectionLength = 0;
            this.inputFilePathTextBox.SelectionStart = 0;
            this.inputFilePathTextBox.ShortcutsEnabled = true;
            this.inputFilePathTextBox.UseSelectable = true;
            this.inputFilePathTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.inputFilePathTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // inputFileExtensionTextBox
            // 
            // 
            // 
            // 
            this.inputFileExtensionTextBox.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            this.inputFileExtensionTextBox.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode2")));
            this.inputFileExtensionTextBox.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location2")));
            this.inputFileExtensionTextBox.CustomButton.Name = "";
            this.inputFileExtensionTextBox.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size2")));
            this.inputFileExtensionTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.inputFileExtensionTextBox.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex2")));
            this.inputFileExtensionTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.inputFileExtensionTextBox.CustomButton.UseSelectable = true;
            this.inputFileExtensionTextBox.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible2")));
            this.inputFileExtensionTextBox.Lines = new string[0];
            resources.ApplyResources(this.inputFileExtensionTextBox, "inputFileExtensionTextBox");
            this.inputFileExtensionTextBox.MaxLength = 32767;
            this.inputFileExtensionTextBox.Name = "inputFileExtensionTextBox";
            this.inputFileExtensionTextBox.PasswordChar = '\0';
            this.inputFileExtensionTextBox.ReadOnly = true;
            this.inputFileExtensionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.inputFileExtensionTextBox.SelectedText = "";
            this.inputFileExtensionTextBox.SelectionLength = 0;
            this.inputFileExtensionTextBox.SelectionStart = 0;
            this.inputFileExtensionTextBox.ShortcutsEnabled = true;
            this.inputFileExtensionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.inputFileExtensionTextBox.UseSelectable = true;
            this.inputFileExtensionTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.inputFileExtensionTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            resources.ApplyResources(this.metroLabel3, "metroLabel3");
            this.metroLabel3.Name = "metroLabel3";
            // 
            // metroLabel4
            // 
            resources.ApplyResources(this.metroLabel4, "metroLabel4");
            this.metroLabel4.Name = "metroLabel4";
            // 
            // downloadButton
            // 
            resources.ApplyResources(this.downloadButton, "downloadButton");
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.UseSelectable = true;
            this.downloadButton.Click += new System.EventHandler(this.DownloadButton_Click_1);
            // 
            // exitButton
            // 
            resources.ApplyResources(this.exitButton, "exitButton");
            this.exitButton.Name = "exitButton";
            this.exitButton.UseSelectable = true;
            this.exitButton.Click += new System.EventHandler(this.ExitButton_Click_1);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.metroTile1, "metroTile1");
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.UseSelectable = true;
            // 
            // clearmainTextBox
            // 
            resources.ApplyResources(this.clearmainTextBox, "clearmainTextBox");
            this.clearmainTextBox.Name = "clearmainTextBox";
            this.clearmainTextBox.UseSelectable = true;
            this.clearmainTextBox.Click += new System.EventHandler(this.ClearmainTextBox_Click);
            // 
            // openDownloadFolder
            // 
            resources.ApplyResources(this.openDownloadFolder, "openDownloadFolder");
            this.openDownloadFolder.Name = "openDownloadFolder";
            this.openDownloadFolder.UseSelectable = true;
            this.openDownloadFolder.Click += new System.EventHandler(this.OpenDownloadFolder_Click);
            // 
            // checkBox01
            // 
            resources.ApplyResources(this.checkBox01, "checkBox01");
            this.checkBox01.Name = "checkBox01";
            this.checkBox01.UseSelectable = true;
            this.checkBox01.CheckedChanged += new System.EventHandler(this.CheckBox01_CheckedChanged);
            // 
            // advancedDownload
            // 
            resources.ApplyResources(this.advancedDownload, "advancedDownload");
            this.advancedDownload.Name = "advancedDownload";
            this.advancedDownload.Style = MetroFramework.MetroColorStyle.Blue;
            this.advancedDownload.UseSelectable = true;
            this.advancedDownload.Click += new System.EventHandler(this.AdvancedDownload_Click);
            // 
            // advancedOpenDownloadFolder
            // 
            resources.ApplyResources(this.advancedOpenDownloadFolder, "advancedOpenDownloadFolder");
            this.advancedOpenDownloadFolder.Name = "advancedOpenDownloadFolder";
            this.advancedOpenDownloadFolder.UseSelectable = true;
            this.advancedOpenDownloadFolder.Click += new System.EventHandler(this.AdvancedOpenDownloadFolder_Click);
            // 
            // mainTextBox
            // 
            // 
            // 
            // 
            this.mainTextBox.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            this.mainTextBox.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location3")));
            this.mainTextBox.CustomButton.Name = "";
            this.mainTextBox.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size3")));
            this.mainTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mainTextBox.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex3")));
            this.mainTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mainTextBox.CustomButton.UseSelectable = true;
            this.mainTextBox.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible3")));
            this.mainTextBox.Lines = new string[0];
            resources.ApplyResources(this.mainTextBox, "mainTextBox");
            this.mainTextBox.MaxLength = 32767;
            this.mainTextBox.Multiline = true;
            this.mainTextBox.Name = "mainTextBox";
            this.mainTextBox.PasswordChar = '\0';
            this.mainTextBox.ReadOnly = true;
            this.mainTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mainTextBox.SelectedText = "";
            this.mainTextBox.SelectionLength = 0;
            this.mainTextBox.SelectionStart = 0;
            this.mainTextBox.ShortcutsEnabled = true;
            this.mainTextBox.UseSelectable = true;
            this.mainTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mainTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // testButton
            // 
            resources.ApplyResources(this.testButton, "testButton");
            this.testButton.Name = "testButton";
            this.testButton.UseSelectable = true;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YoutubeVideoDownloaderInForms.Properties.Resources.parade_black_60x60;
            this.BackImage = global::YoutubeVideoDownloaderInForms.Properties.Resources.parade_black_60x60;
            this.Controls.Add(this.testButton);
            this.Controls.Add(this.mainTextBox);
            this.Controls.Add(this.advancedOpenDownloadFolder);
            this.Controls.Add(this.advancedDownload);
            this.Controls.Add(this.checkBox01);
            this.Controls.Add(this.openDownloadFolder);
            this.Controls.Add(this.clearmainTextBox);
            this.Controls.Add(this.metroTile1);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.downloadButton);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.inputFileExtensionTextBox);
            this.Controls.Add(this.inputFilePathTextBox);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.downloadFolderPathTextBox);
            this.Controls.Add(this.selectInputFile);
            this.Controls.Add(this.selectDownloadFolder);
            this.Name = "Form1";
            this.Resizable = false;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public MetroFramework.Controls.MetroButton selectDownloadFolder;
        public MetroFramework.Controls.MetroButton selectInputFile;
        public MetroFramework.Controls.MetroTextBox downloadFolderPathTextBox;
        public MetroFramework.Controls.MetroLabel metroLabel1;
        public MetroFramework.Controls.MetroLabel metroLabel2;
        public MetroFramework.Controls.MetroTextBox inputFilePathTextBox;
        public MetroFramework.Controls.MetroTextBox inputFileExtensionTextBox;
        public MetroFramework.Controls.MetroLabel metroLabel3;
        public MetroFramework.Controls.MetroLabel metroLabel4;
        public MetroFramework.Controls.MetroButton downloadButton;
        public MetroFramework.Controls.MetroButton exitButton;
        public MetroFramework.Controls.MetroTile metroTile1;
        public MetroFramework.Controls.MetroButton clearmainTextBox;
        public MetroFramework.Controls.MetroButton openDownloadFolder;
        public MetroFramework.Controls.MetroCheckBox checkBox01;
        public MetroFramework.Controls.MetroButton advancedDownload;
        public MetroFramework.Controls.MetroButton advancedOpenDownloadFolder;
        public MetroFramework.Controls.MetroTextBox mainTextBox;
        private MetroFramework.Controls.MetroButton testButton;
    }
}

