﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using YoutubeExtractor;

namespace YoutubeVideoDownloaderInForms
{
    class AdvancedYoutubeDownloader : Form1
    {
        private double processPercentage;

        public double ProcessPercentage { get => processPercentage; set => processPercentage = value; }
        public string GetVideoTitile { get; private set; }

        public List<string> VideoTitleList = new List<string>();


        public void DownloadVideo(IEnumerable<VideoInfo> videoInfos)
        {
            /*
             * Select the first .mp4 video with 360p resolution
             */
            VideoInfo video = videoInfos
                .First(info => info.VideoType == VideoType.Mp4 && info.Resolution == 360);

            /*
             * If the video has a decrypted signature, decipher it
             */
            if (video.RequiresDecryption)
            {
                DownloadUrlResolver.DecryptDownloadUrl(video);
            }

            /*
             * Create the video downloader.
             * The first argument is the video to download.
             * The second argument is the path to save the video file.
             */
            try
            {
                var videoDownloader = new VideoDownloader(video,
                Path.Combine(Path.Combine(DownloadFolderAdvanced + "\\" + AdvancedGetDownloadFolderFromConfig(), RemoveIllegalPathCharacters(video.Title) + video.VideoExtension)));
                GetVideoTitile = video.Title;


                //Register the ProgressChanged event and print the current progress
                videoDownloader.DownloadProgressChanged += (sender, args) => Console.WriteLine(args.ProgressPercentage);
        

                /*
                 * Execute the video downloader.
                 * For GUI applications note, that this method runs synchronously.
                 */
                videoDownloader.Execute();

            }

            catch (DirectoryNotFoundException ex03)
            {
                Console.WriteLine(ex03);
            }

        }
        public string RemoveIllegalPathCharacters(string path)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(path, "");
        }
    }
}
