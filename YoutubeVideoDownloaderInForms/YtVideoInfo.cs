﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YoutubeVideoDownloaderInForms
{
    public class YtVideoInfo
    {
        public string Link { get; set; }
        public string VideoTitle { get; set; }
        public double VideoDownloadTime { get; set; }
    }

}
