﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using YoutubeExtractor;

namespace YoutubeVideoDownloaderInForms
{
    
     class YoutubeDownloader : Form1
    {

        private static string getVideoTitle;
        private double processPercentage;

        public double ProcessPercentage { get => processPercentage; set => processPercentage = value; }
        public string GetVideoTitle { get => getVideoTitle; set => getVideoTitle = value; }
        

        public void DownloadVideo(IEnumerable<VideoInfo> videoInfos)
        {
            /*
             * Select the first .mp4 video with 360p resolution
             */
            VideoInfo video = videoInfos
                .First(info => info.VideoType == VideoType.Mp4 && info.Resolution == 360);
        
            /*
             * If the video has a decrypted signature, decipher it
             */
            if (video.RequiresDecryption)
            {
                DownloadUrlResolver.DecryptDownloadUrl(video);
            }

            /*
             * Create the video downloader.
             * The first argument is the video to download.
             * The second argument is the path to save the video file.
             */
            try
            {
                //This will create directory if it not exist and it will not bug if directory already exist.
                //Directory.CreateDirectory(DownloadFolder);
                var videoDownloader = new VideoDownloader(video,
                Path.Combine(DownloadFolder, RemoveIllegalPathCharacters(video.Title) + video.VideoExtension));
                GetVideoTitle = video.Title;

                //Register the ProgressChanged event and print the current progress
                videoDownloader.DownloadProgressChanged += (sender, args) => Console.WriteLine(args.ProgressPercentage);
                

                /*
                 * Execute the video downloader.
                 * For GUI applications note, that this method runs synchronously.
                 */
                videoDownloader.Execute();

            }

            catch (DirectoryNotFoundException ex03)
            {
                Console.WriteLine(ex03);
            }

        }
        public string RemoveIllegalPathCharacters(string path)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(path, "");
        }
    }
}
