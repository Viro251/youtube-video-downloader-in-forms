﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YoutubeVideoDownloaderInForms
{
    interface IAdvancedDownload
    {
        string AdvancedGetInputFileExtensionFromConfig();
        void AdvancedSetSetInputFilePathTextBox();

        string AdvancedGetDownloadFolderFromConfig();
        void AdvancedSetSetDownloadFolderTextBox();

        void AdvancedDownloadFilesFromJson();
        void AdvamcedDownloadFilesFromTxt();
        void AdvancedDownloadFilesFromXml();
    }
}
