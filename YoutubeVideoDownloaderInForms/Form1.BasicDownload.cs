﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using YoutubeExtractor;

namespace YoutubeVideoDownloaderInForms
{
    //Basic part of class
    public partial class Form1
    {
        //Model
        public static string DownloadFolder { get; set; }
        public string SelectedInputFilePath { get; set; }
        public string SelectedInputFileExtension { get; set; }
        public string PathToSelectedInputFile { get; set; }
        public string SelectedInputFileType { get; set; }
        public string EveryFileDownloadTime { get; set; }
        public string YoutubeVideoTitle { get; set; }

        //View
        private void SetDownloadFolderPathTextBox()
        {
            try { downloadFolderPathTextBox.Text = DownloadFolder; }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        public void SetInputFilePathTextBox()
        {
            try { inputFilePathTextBox.Text = SelectedInputFilePath; }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        public void SetInputFileExtensionTextBox()
        {
            try { inputFileExtensionTextBox.Text = SelectedInputFileExtension; }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        public void WriteElapsedTimeToMainTextBox()
        {
            try { mainTextBox.Text = EveryFileDownloadTime; }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        public void WriteVideoTitileToMainTextBox()
        {
            try
            {
                YoutubeDownloader youtubeDownloader = new YoutubeDownloader();
                YoutubeVideoTitle = youtubeDownloader.GetVideoTitle;
                mainTextBox.Text = YoutubeVideoTitle;
            }

            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        //Basic Download
        public void DownloadFilesFromJson()
        {
                //Reading json part
                List<Link> ytLinks = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Link>>(File.ReadAllText(SelectedInputFilePath));
                Stopwatch swWholeLoop = Stopwatch.StartNew();
            try
            {
                Parallel.ForEach(ytLinks, new ParallelOptions { MaxDegreeOfParallelism = 5 }, links =>
                {
                    try
                    {
                        Stopwatch sw = Stopwatch.StartNew();
                        IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(links.link, false);
                        YoutubeDownloader _youtubeDownloader = new YoutubeDownloader();
                        _youtubeDownloader.DownloadVideo(videoInfos);
                        mainTextBox.Text += Environment.NewLine + _youtubeDownloader.GetVideoTitle;
                        EveryFileDownloadTime = "Downloading all files finished in: " + (swWholeLoop.ElapsedMilliseconds / 1000.0) + " seconds";
                        mainTextBox.Text += Environment.NewLine + EveryFileDownloadTime;
                    }
                    catch (AggregateException ae)
                    {
                        ae.Handle((x) =>
                        {
                            if (x is WebException)
                            {
                                MessageBox.Show("403 forbidden" + " " + links.link);
                                return true;
                            }
                            else if (x is YoutubeParseException)
                            {
                                MessageBox.Show("Youtube parser exception" + " " + links.link);
                                return true;
                            }
                            return false;
                        });
                    }



                });
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    if (x is WebException)
                    {
                        MessageBox.Show("403 forbidden");
                        return true;
                    }
                    else if (x is YoutubeParseException)
                    {
                        MessageBox.Show("Youtube parser exception");
                        return true;
                    }
                    return false;
                });
            }




            //catch (AggregateException ae)
            //{
            //    ae.Handle((x) =>
            //    {
            //        if (x is WebException)
            //        {
            //            MessageBox.Show("403 forbidden");
            //            return true;
            //        }
            //        else if (x is YoutubeParseException)
            //        {
            //            MessageBox.Show("Youtube parser exception");
            //            return true;
            //        }
            //        return false;
            //    });
            //}
        }
        public void DownloadFilesFromTxt()
        {
            try
            {
                //Reading txt part
                string[] links = File.ReadAllLines(SelectedInputFilePath);

                Stopwatch swWholeLoop = Stopwatch.StartNew();

                Parallel.ForEach(links, new ParallelOptions { MaxDegreeOfParallelism = 5 }, line =>
                {
                    //Time flow meter
                    Stopwatch sw = Stopwatch.StartNew();

                    IEnumerable<VideoInfo> videoInfo = DownloadUrlResolver.GetDownloadUrls(line, false);
                    YoutubeDownloader _youtubeDownloader = new YoutubeDownloader();
                    _youtubeDownloader.DownloadVideo(videoInfo);


                });
                EveryFileDownloadTime = "Downloading all files finished in: " + swWholeLoop.ElapsedMilliseconds / 1000.0 + " seconds";
                WriteElapsedTimeToMainTextBox();
            }

            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    if (x is WebException)
                    {
                        MessageBox.Show("403 forbidden");
                        return true;
                    }
                    else if (x is YoutubeParseException)
                    {
                        MessageBox.Show("Youtube parser exception");
                        return true;
                    }
                    return false;
                });
            }
        }
        public void DownloadFilesFromXml()
        {
            try
            {
                //Reading xml part
                XmlDocument linksXml = new XmlDocument();
                linksXml.Load(SelectedInputFilePath);
                XmlNodeList linksList = linksXml.SelectNodes("/root/link");

                Stopwatch swWholeLoop = Stopwatch.StartNew();

                Parallel.ForEach(linksList.Cast<XmlNode>(), new ParallelOptions { MaxDegreeOfParallelism = 5 }, (XmlNode link) =>
                {

                    Stopwatch sw = Stopwatch.StartNew();

                    IEnumerable<VideoInfo> videoInfo = DownloadUrlResolver.GetDownloadUrls(link.InnerText, false);
                    YoutubeDownloader _youtubeDownloader = new YoutubeDownloader();
                    _youtubeDownloader.DownloadVideo(videoInfo);

                });
                EveryFileDownloadTime = "Downloading all files finished in: " + (swWholeLoop.ElapsedMilliseconds / 1000.0) + " seconds";
                WriteElapsedTimeToMainTextBox();
            }

            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    if (x is WebException)
                    {
                        MessageBox.Show("403 forbidden");
                        return true;
                    }
                    else if (x is YoutubeParseException)
                    {
                        MessageBox.Show("Youtube parser exception");
                        return true;
                    }
                    return false;
                });
            }
        }

        //Buttons events handlers

        //Select download folder button
        private void SelectDownloadFolder_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.RootFolder = Environment.SpecialFolder.MyComputer;
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    DownloadFolder = fbd.SelectedPath;
                }
                fbd.Description = "Select download folder";
                fbd.RootFolder = Environment.SpecialFolder.Desktop;
                SetDownloadFolderPathTextBox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        //Select folder button
        private void SelectDownloadFolder_Click_1(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.RootFolder = Environment.SpecialFolder.MyComputer;
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    DownloadFolder = fbd.SelectedPath;
                }
                fbd.Description = "Select download folder";
                fbd.RootFolder = Environment.SpecialFolder.Desktop;
                SetDownloadFolderPathTextBox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        //Select input file button
        private void SelectInputFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog opf = new OpenFileDialog();
                opf.Filter = "json file (*.json)|*.json|txt file (*.txt)|*.txt|xml file (*.xml)|*.xml";
                opf.Title = "Select input file that contains youtube links";
                opf.ShowDialog();
                opf.InitialDirectory = "c:\\";
                SelectedInputFilePath = opf.FileName;
                SelectedInputFileExtension = Path.GetExtension(opf.FileName);
                SetInputFilePathTextBox();
                SetInputFileExtensionTextBox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        //Exit button
        private void ExitButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        //Download button
        private async void DownloadButton_Click_1(object sender, EventArgs e)
        {
            if (SelectedInputFileExtension == ".json")
            {
                Task task = new Task(DownloadFilesFromJson);
                task.Start();
                await task;
                mainTextBox.Text += Environment.NewLine + GetCurrentThreadId() + Environment.NewLine;
            }
            else if (SelectedInputFileExtension == ".txt")
            {
                Task task = new Task(DownloadFilesFromTxt);
                task.Start();
                await task;
                mainTextBox.Text += Environment.NewLine + GetCurrentThreadId() + Environment.NewLine;
            }
            else if (SelectedInputFileExtension == ".xml")
            {
                Task task = new Task(DownloadFilesFromXml);
                task.Start();
                await task;
                mainTextBox.Text += Environment.NewLine + GetCurrentThreadId() + Environment.NewLine;
            }

        }
        //Clear main text box button
        private void ClearmainTextBox_Click(object sender, EventArgs e)
        {
            try
            {
                mainTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        //Open Download folder button
        private void OpenDownloadFolder_Click(object sender, EventArgs e)
        {
            if (DownloadFolder == null)
            {
                try
                {
                    MessageBox.Show("Download folder is not selected.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            else
            {
                try
                {
                    Process.Start("explorer.exe", DownloadFolder);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

    }
}

