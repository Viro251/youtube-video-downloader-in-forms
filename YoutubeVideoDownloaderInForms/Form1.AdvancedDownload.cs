﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using YoutubeExtractor;
using System.Net;

namespace YoutubeVideoDownloaderInForms
{
    //Advanced part of class
    public partial class Form1
    {
        //Lists
        public List<YtVideoInfo> YtVideoInfoList = new List<YtVideoInfo>();
        public List<string> RequestedYtVideoTitlesList = new List<string>();
        public List<string> MoviesThatHaveBeenDownloaded = new List<string>();
        public IEnumerable<string> MismatchingList = new List<string>();

        //Paths
        public string ConfigXmlPath { get; } = Path.Combine(Environment.CurrentDirectory + "\\userfiles" + "\\config.xml");
        public string JsonLinkPath { get; } = Path.Combine(Environment.CurrentDirectory + "\\userfiles" + "\\links.json");
        public string TxtLinkPath { get; } = Path.Combine(Environment.CurrentDirectory + "\\userfiles" + "\\links.txt");
        public string XmlLinkPath { get; } = Path.Combine(Environment.CurrentDirectory + "\\userfiles" + "\\links.xml");
        public string DownloadFolderAdvanced { get; } = Path.Combine(Environment.CurrentDirectory + "\\download");
        public string AllDownloadTime { get; private set; }
        public string SetDownloadFolder { get; }
        public string WebException { get; set; }
        public string YoutubeParseException { get; set; }

        //List Methods
        private void AddDownloadedMoviesToDownloadList()
        {
            try
            {
                var moviesInDownloadFolder = Directory.GetFiles(DownloadFolderAdvanced  + "\\" + AdvancedGetDownloadFolderFromConfig()).Select(Path.GetFileNameWithoutExtension);
                foreach (string movie in moviesInDownloadFolder)
                {
                    MoviesThatHaveBeenDownloaded.Add(movie);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        private void AddVideoTitleFromGenericListToStringList()
        {
            //This method selects video titles from YtVideoInfoList and put them into YtVideoTitlesList
            foreach (var _object in YtVideoInfoList)
            {
                RequestedYtVideoTitlesList.Add(_object.VideoTitle);
            }
        }
        private void CompareListsAndSetMismatchingInToMismatchingList()
        {
            MismatchingList = RequestedYtVideoTitlesList.Except(MoviesThatHaveBeenDownloaded);
        }

        //Other Methods
        private void ShowExceptions()
        {
            try
            {
                if (WebException != null)
                {
                    MessageBox.Show(WebException);
                }
                if (YoutubeParseException != null)
                {
                    MessageBox.Show(YoutubeParseException);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        private string GetCurrentThreadId()
        {
            try
            {
                string CurrentThread = Thread.CurrentThread.ManagedThreadId.ToString();
                return "Current thread id: " + CurrentThread;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return ex.ToString();
            }
        }
        private void AdvancedCreateMissingDownloadFolder()
        {
            try
            {
                Directory.CreateDirectory(Path.Combine(DownloadFolderAdvanced + "\\" + AdvancedGetDownloadFolderFromConfig()));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //Methods that write into TextBox
        private void WriteTargetedAndDownloadedMoviesToMainTextBox()
        {
            if (YoutubeParseException != null)
            {
                mainTextBox.Text += "Warning: The Youtube parser error has occured so below video titiles will not match specified movies in input file";
            }
            mainTextBox.Text += "Selected videos to download: " + Environment.NewLine;
            foreach (var videoTitle in RequestedYtVideoTitlesList)
            {
                mainTextBox.Text += videoTitle + Environment.NewLine;
            }
            mainTextBox.Text += Environment.NewLine + "Correctly downloaded: " + Environment.NewLine;
            foreach (string movieName in MoviesThatHaveBeenDownloaded)
            {
                mainTextBox.Text += movieName + Environment.NewLine;
            }
            mainTextBox.Text += Environment.NewLine;
        }
        private void WriteAllDownloadTimeToMainTextBox()
        {
            try
            {
                mainTextBox.Text += AllDownloadTime + Environment.NewLine;
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        private void WriteTitileDownloadTimeAndLinkToMainTextBox()
        {
            try
            {
                foreach (var link in YtVideoInfoList)
                {
                    mainTextBox.Text
                        += Environment.NewLine
                        + "Movie title: " + link.VideoTitle + " "
                        + "download time: " + link.VideoDownloadTime + " "
                        + "link: " + link.Link + Environment.NewLine + Environment.NewLine;
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        private void WriteExceptions()
        {
            try
            {
                if (WebException != null)
                {
                    mainTextBox.Text += WebException;
                }
                if (YoutubeParseException != null)
                {
                    mainTextBox.Text += YoutubeParseException;
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        private void WriteMismatchings()
        {
            try
            {
                if (MismatchingList != null)
                {
                    mainTextBox.Text += "The following videos have not been downloaded!" + Environment.NewLine;
                    foreach (var mismatch in MismatchingList)
                    {
                        mainTextBox.Text += mismatch;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //Buttons
        private void CheckBox01_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (checkBox01.Checked == true)
                {
                    AdvancedCreateMissingDownloadFolder();
                    advancedOpenDownloadFolder.Visible = true;

                    downloadButton.Visible = false;
                    openDownloadFolder.Visible = false;
                    selectInputFile.Visible = false;
                    selectDownloadFolder.Visible = false;
                    metroLabel2.Visible = false;
                    inputFilePathTextBox.Visible = false;
                    AdvancedSetSetInputFilePathTextBox();
                    AdvancedSetSetDownloadFolderTextBox();
                    advancedDownload.Visible = true;
                }
                if (checkBox01.Checked == false)
                {
                    advancedOpenDownloadFolder.Visible = false;

                    downloadButton.Visible = true;
                    openDownloadFolder.Visible = true;
                    selectInputFile.Visible = true;
                    selectDownloadFolder.Visible = true;
                    metroLabel2.Visible = true;
                    inputFilePathTextBox.Visible = true;
                    advancedDownload.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private async void AdvancedDownload_Click(object sender, EventArgs e)
        {

            if (AdvancedGetInputFileExtensionFromConfig() == ".json")
            {
                Task task = new Task(AdvancedDownloadFilesFromJson);
                task.Start();
                await task;
                WriteAllDownloadTimeToMainTextBox();
                WriteTitileDownloadTimeAndLinkToMainTextBox();
                ShowExceptions();
                WriteExceptions();
                AddDownloadedMoviesToDownloadList();
                AddVideoTitleFromGenericListToStringList();
                WriteTargetedAndDownloadedMoviesToMainTextBox();
                CompareListsAndSetMismatchingInToMismatchingList();
                WriteMismatchings();

            }
            else if (AdvancedGetInputFileExtensionFromConfig() == ".txt")
            {
                Task task = new Task(AdvamcedDownloadFilesFromTxt);
                task.Start();
                await task;
                WriteAllDownloadTimeToMainTextBox();
                WriteTitileDownloadTimeAndLinkToMainTextBox();
                ShowExceptions();
                WriteExceptions();
            }
            else if (AdvancedGetInputFileExtensionFromConfig() == ".xml")
            {
                Task task = new Task(AdvancedDownloadFilesFromXml);
                task.Start();
                await task;
                WriteAllDownloadTimeToMainTextBox();
                WriteTitileDownloadTimeAndLinkToMainTextBox();
                ShowExceptions();
                WriteExceptions();
            }
            else if (AdvancedGetInputFileExtensionFromConfig() != ".json" || AdvancedGetInputFileExtensionFromConfig() != ".txt" || AdvancedGetInputFileExtensionFromConfig() != ".xml")
            { MessageBox.Show("Warning. There is a wrong input value in config!"); }
        }
        private void AdvancedOpenDownloadFolder_Click(object sender, EventArgs e)
        {
            try
            {
                if (AdvancedGetDownloadFolderFromConfig() == null)
                {
                    MessageBox.Show("Download folder is not selected.");
                }
                else
                {
                    Process.Start("explorer.exe", Path.Combine(DownloadFolderAdvanced + "\\" + AdvancedGetDownloadFolderFromConfig()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void testButton_Click(object sender, EventArgs e)
        {
            try
            {
                WriteTitileDownloadTimeAndLinkToMainTextBox();
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        //Read config methods
        private string AdvancedGetInputFileExtensionFromConfig()
        {
            try
            {
                XmlDocument configXml = new XmlDocument();
                configXml.Load(ConfigXmlPath);
                XmlNode root = configXml.FirstChild;
                string AdvancedInputFileType = root["inputType"].InnerText;
                return AdvancedInputFileType;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return ex.ToString();
            }

        }
        private void AdvancedSetSetInputFilePathTextBox()
        {
            try { inputFileExtensionTextBox.Text = AdvancedGetInputFileExtensionFromConfig(); }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }

        }
        private string AdvancedGetDownloadFolderFromConfig()
        {
            try
            {
                XmlDocument configXml = new XmlDocument();
                configXml.Load(ConfigXmlPath);
                XmlNode root = configXml.FirstChild;
                string AdvancedDownloadFolder = root["downloadFolder"].InnerText;
                return AdvancedDownloadFolder;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return ex.ToString();
            }
        }
        private void AdvancedSetSetDownloadFolderTextBox()
        {
            try { downloadFolderPathTextBox.Text = Path.Combine(DownloadFolderAdvanced + "\\" + AdvancedGetDownloadFolderFromConfig()); }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        //Advanced download methods
        private void AdvancedDownloadFilesFromJson()
        {
            //Reading json part
            List<Link> ytLinks = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Link>>(File.ReadAllText(JsonLinkPath));
            Parallel.ForEach(ytLinks, new ParallelOptions { MaxDegreeOfParallelism = 5 }, links =>
            {
                try
                {
                    Stopwatch swForEachLink = Stopwatch.StartNew();
                    IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(links.link, false);
                    AdvancedYoutubeDownloader advancedYoutubeDownloader = new AdvancedYoutubeDownloader();
                    try
                    {
                        advancedYoutubeDownloader.DownloadVideo(videoInfos);
                    }
                    catch (WebException ex)
                    {
                        WebException =
                        "Warning: This video is protected by the server and can not be downloaded!" + Environment.NewLine + Environment.NewLine
                        + "Title of the video that caused error: " + Environment.NewLine + advancedYoutubeDownloader.GetVideoTitile + Environment.NewLine + Environment.NewLine
                        + "Link of the video that caused error: " + Environment.NewLine + links.link + Environment.NewLine 
                        + "Error description: " + Environment.NewLine + ex.ToString() + Environment.NewLine + Environment.NewLine;
                    }
                    YtVideoInfoList.Add(new YtVideoInfo() { Link = links.link, VideoTitle = advancedYoutubeDownloader.GetVideoTitile, VideoDownloadTime = swForEachLink.ElapsedMilliseconds / 1000.0 });
                }
                catch (YoutubeParseException ex)
                {
                    YoutubeParseException =
                    "Youtube parser error occured." + Environment.NewLine + Environment.NewLine
                    + "Link of the video that caused error: " + links.link + Environment.NewLine + Environment.NewLine
                    + "Exception: " + Environment.NewLine + ex.ToString() + Environment.NewLine + Environment.NewLine;
                }
            });
        }
        //Working catcher but without title and link
        //catch (AggregateException ae)
        //{
        //    ae.Handle((x) =>
        //    {
        //        if (x is WebException)
        //        {
        //            MessageBox.Show("403 forbidden");
        //            return true;
        //        }
        //        else if (x is YoutubeParseException)
        //        {
        //            MessageBox.Show("Youtube parser exception");
        //            return true;
        //        }
        //        return false;
        //    });
        //}
        private void AdvamcedDownloadFilesFromTxt()
        {
            //Reading txt part
            string[] links = File.ReadAllLines(TxtLinkPath);

            Parallel.ForEach(links, new ParallelOptions { MaxDegreeOfParallelism = 5 }, line =>
            {
                try
                {
                    Stopwatch swForEachLink = Stopwatch.StartNew();
                    IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(line, false);
                    AdvancedYoutubeDownloader advancedYoutubeDownloader = new AdvancedYoutubeDownloader();
                    try
                    {
                        advancedYoutubeDownloader.DownloadVideo(videoInfos);
                    }
                    catch (WebException ex)
                    {
                        WebException =
                        "Warning: This video is protected by the server and can not be downloaded!" + Environment.NewLine + Environment.NewLine
                        + "Title of the video that caused error: " + Environment.NewLine + advancedYoutubeDownloader.GetVideoTitile + Environment.NewLine + Environment.NewLine
                        + "Link of the video that caused error: " + Environment.NewLine + line + Environment.NewLine
                        + "Error description: " + Environment.NewLine + ex.ToString() + Environment.NewLine + Environment.NewLine;
                    }
                    YtVideoInfoList.Add(new YtVideoInfo() { Link = line, VideoTitle = advancedYoutubeDownloader.GetVideoTitile, VideoDownloadTime = swForEachLink.ElapsedMilliseconds / 1000.0 });
                }

                catch (YoutubeParseException ex)
                {
                    YoutubeParseException =
                    "Youtube parser error occured." + Environment.NewLine + Environment.NewLine
                    + "Link of the video that caused error: " + line + Environment.NewLine + Environment.NewLine
                    + "Exception: " + Environment.NewLine + ex.ToString() + Environment.NewLine + Environment.NewLine;
                }
            });
        }
        private void AdvancedDownloadFilesFromXml()
        {
                //Reading xml part
                XmlDocument linksXml = new XmlDocument();
                linksXml.Load(XmlLinkPath);
                XmlNodeList linksList = linksXml.SelectNodes("/root/link");
                Parallel.ForEach(linksList.Cast<XmlNode>(), new ParallelOptions { MaxDegreeOfParallelism = 5 }, (XmlNode links) =>
                {
                    try
                    {
                        Stopwatch swForEachLink = Stopwatch.StartNew();
                        IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(links.InnerText, false);
                        AdvancedYoutubeDownloader advancedYoutubeDownloader = new AdvancedYoutubeDownloader();
                        try
                        {
                            advancedYoutubeDownloader.DownloadVideo(videoInfos);
                        }
                        catch (WebException ex)
                        {
                            WebException =
                            "Warning: This video is protected by the server and can not be downloaded!" + Environment.NewLine + Environment.NewLine
                            + "Title of the video that caused error: " + Environment.NewLine + advancedYoutubeDownloader.GetVideoTitile + Environment.NewLine + Environment.NewLine
                            + "Link of the video that caused error: " + Environment.NewLine + links.InnerText + Environment.NewLine
                            + "Error description: " + Environment.NewLine + ex.ToString() + Environment.NewLine + Environment.NewLine;
                        }
                        YtVideoInfoList.Add(new YtVideoInfo() { Link = links.InnerText, VideoTitle = advancedYoutubeDownloader.GetVideoTitile, VideoDownloadTime = swForEachLink.ElapsedMilliseconds / 1000.0 });
                    }
                    catch (YoutubeParseException ex)
                    {
                        YoutubeParseException =
                        "Youtube parser error occured." + Environment.NewLine + Environment.NewLine
                        + "Link of the video that caused error: " + links.InnerText + Environment.NewLine + Environment.NewLine
                        + "Exception: " + Environment.NewLine + ex.ToString() + Environment.NewLine + Environment.NewLine;
                    }
                });
        }
    }
}


