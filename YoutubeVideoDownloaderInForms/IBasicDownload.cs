﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YoutubeVideoDownloaderInForms
{
    interface IBasicDownload
    {
        void DownloadFilesFromJson();
        void DownloadFilesFromTxt();
        void DownloadFilesFromXml();
    }
}
